pytest-ligo
~~~~~~~~~~~

Py.test plugin which adds a ligo fixture to encapsulate boilerplate business
logic. Refer to ``test_pytest_ligo.py`` for example usage.

Ex.:
    pip install -e .
    pytest-3 examples/test_pytest_ligo.py

Install with pip, ie::

    pip install pytest-ligo

You also need ligo, which you can install in ~/.local/bin with::

    curl https://gitlab.com/jpic/pytest-ligo/raw/master/install.sh | bash -eux -

To use the above, docker should be installed and configured on your system.
