from setuptools import setup


setup(
    name='pytest-ligo',
    versioning='dev',
    setup_requires='setupmeta',
    author='James Pic',
    author_email='jamespic@gmail.com',
    url='https://yourlabs.io/oss/pytest-ligo',
    include_package_data=True,
    license='MIT',
    keywords='cli ligo',
    install_requires=[
        'typed-ast==1.4.0',
    ],
    python_requires='>=3.6',

)
