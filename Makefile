TYPECHECK=mypy
# pytest causes redefined-outer-name and no-self-use
LINT=pylint -d redefined-outer-name,no-self-use,missing-docstring,too-many-arguments,too-few-public-methods,fixme,too-many-locals,too-many-public-methods,too-many-instance-attributes,R0801,c-extension-no-member
LINT2=pycodestyle
PACKAGES=pytest_ligo
LOG_DIR=tmp

lint_all: lint lint2

typecheck:
	$(TYPECHECK) $(PACKAGES)

lint:
	@echo "Linting with pylint, version:"
	@pylint --version | sed 's/^/  /'
	$(LINT) $(PACKAGES)

lint2:
	@echo "Linting with pycodestyle version `pycodestyle --version`"
	$(LINT2) $(PACKAGES)

clean:
	rm -rf tmp/*  __pycache__ *.pyc */__pycache__ */*.pyc .mypy_cache .pytest_cache .pytype
