import os
import subprocess
import json
from pathlib import Path
from typing import List
import pytest


def format_command(cmd: List[str]) -> str:
    color_code = '\033[34m'
    endc = '\033[0m'
    cmd_str = " ".join(cmd)
    return f'{color_code}# {cmd_str}{endc}'


class Contract:
    def __init__(self, ligo, path):
        self.ligo = ligo
        self.code = json.loads(subprocess.check_output(f'''
            {ligo.bin} compile-contract --michelson-format=json {path} main
        '''.strip(), shell=True))


class Ligo:
    @property
    def bin(self):
        own = Path(os.getenv('HOME')) / '.local' / 'bin' / 'ligo'
        if own.exists():
            return str(own)
        try:
            output = subprocess.check_output('which ligo', shell=True)
            return output.decode('utf8').strip()
        except subprocess.CalledProcessError:
            print('ligo not available, try install.sh')

    def run(self, params: List[str]):
        cmd = [self.bin] + params
        print(format_command(cmd))
        return subprocess.check_output(cmd).decode('utf8')

    def compile(self, contract: str, entrypoint: str):
        res = self.run(
            ['compile-contract',
             '--michelson-format=json',
             contract,
             entrypoint])
        return json.loads(res)

    def run_function(self, params):
        res = self.run(["run-function", "--format", "json"] + params)
        return json.loads(res)

    def compile_storage(self, contract: str, entrypoint: str, storage: str):
        res = self.run(["compile-storage", "--format",
                        "json", contract, entrypoint, storage])
        return json.loads(res)


@pytest.fixture
def ligo():
    return Ligo()
