import os
import pytest

cur_path = os.path.dirname(os.path.realpath(__file__))
path = os.path.dirname(cur_path) + '/'

@pytest.mark.ligo
class TestLigo:
    """Test ."""

    def test_compile(self,ligo):
        contract = os.path.join(path,"examples", "contract.mligo")
        assert ligo.compile(contract,"main")

    def test_storage(self,ligo):
        contract = os.path.join(path,"examples", "contract.mligo")
        res = ligo.compile_storage(contract,"main","0n")
        assert res['content'] == "0\n"
