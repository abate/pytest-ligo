"""Hooks and fixtures.

A fixture defines code to be run before and after a (sequence of) test,
E.g. start and stop a server. The fixture is simply specified as a parameter
in the test function, and the yielded values is then accessible with this
parameter.
"""
import os
import pytest
from pytest_ligo.ligo import Ligo

@pytest.fixture(scope="session")
def log_dir(request):
    """Retrieve user-provided logging directory on the command line."""
    yield request.config.getoption("--log-dir")

@pytest.fixture(scope="class")
def session():
    """Dictionary to store data between tests."""
    yield {}

# @pytest.fixture(scope="session", autouse=True)
# def sanity_check(request):
    # """Sanity checks before running the tests."""
    # log_dir = request.config.getoption("--log-dir")
    # if not (log_dir is None or os.path.isdir(log_dir)):
       # print(f"{log_dir} doesn't exist")
       # pytest.exit(1)

@pytest.fixture(scope="class")
def ligo():
    yield Ligo()
