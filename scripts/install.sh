#!/bin/sh -eux
mkdir -p ~/.local/bin
rm -f ~/.local/bin/ligo
cat <<EOF > ~/.local/bin/ligo
#!/bin/sh
docker run --user=`id -u` -v \$PWD:\$PWD -w \$PWD ligolang/ligo:next "\$@"
EOF
chmod +x ~/.local/bin/ligo
touch ~/.bash_profile
if ! grep local/bin ~/.bash_profile; then
    echo 'export PATH="$HOME/.local/bin:$PATH"' > ~/.bash_profile
fi
